<?php

namespace App\Http\Controllers\Api;

use App\Mail\Registration;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Mailer\Transport;
use App\Http\Responses\Api\ApiResponse;
use PulkitJalan\Google\Facades\Google;
use App\Http\Controllers\Controller;
use Symfony\Component\Mailer\Mailer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Mime\Email;
use Illuminate\Validation\Rule;
use PulkitJalan\Google\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Google\Service\Oauth2;
use App\Models\User;

class RegistrationController extends Controller
{
    private string $appKey = "yiwdnlffqseecpjl";

    private function getClient(): \Google\Client
    {
        $client = new \Google\Client();

        $client->setClientId('313960232579-mllpsl484fn0ri6liht8si29jev3lqtb.apps.googleusercontent.com');
        $client->setClientSecret('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');
        $client->setRedirectUri('http://127.0.0.1:8000/back');
        $client->setScopes('email');
        $client->setIncludeGrantedScopes(true);

        return $client;
    }

    public function login()
    {
        try {
            $client = $this->getClient();

            $url = $client->createAuthUrl();

            return app(ApiResponse::class)->success($url, 'Success');
        } catch (\Exception $e) {
            Log::error($e);

            return app(ApiResponse::class)->error('Something went wrong.');
        }
    }

    public function getToken(Request $request)
    {
        try {
            $client = $this->getClient();
            $code = data_get($request, 'code');

            $data = $client->fetchAccessTokenWithAuthCode($code);

            return app(ApiResponse::class)->success($data, 'Access Token');
        } catch (\Exception $e) {
            Log::error($e);

            return app(ApiResponse::class)->error('Something went wrong.');
        }
    }

    public function post(Request $request)
    {
        DB::beginTransaction();

        $validator = validator::make($request->all(), [
                        'email' => ['required', 'email', Rule::unique('users')],
                        'access_token' => ['required', 'string'],
                    ], [
                        'email.unique' => 'You have already registered. Please try again...',
                    ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->errors()->all());

            return resolve(ApiResponse::class)->error($message);
        }

        try  {
            $token = data_get($request, 'access_token');

            if (! $this->isValidToken($token)) {
                return app(ApiResponse::class)->error('Something went wrong. Please try again...');
            }

            $client = $this->getClient();

            $client->setAccessToken($token);

            $service = new Oauth2($client);

            $clientInfo = $service->userinfo->get();
            $clientEmail = data_get($clientInfo, 'email');

            $transport = Transport::fromDsn('gmail+smtp://'.urlencode($clientEmail).':'.urlencode($this->appKey).'@default');

            $mailer = new Mailer($transport);

            $message = (new Email())
                        ->from($clientEmail)
                        ->to(data_get($request, 'email'))
                        ->subject('New Registration')
                        ->html('<h4>Your registration has been successful.</h4>');

            User::create([
                'name' => Str::random(4),
                'email' => data_get($request, 'email'),
                'password' => Hash::make('!52$71'),
            ]);

            $mailer->send($message);

            DB::Commit();

            return app(ApiResponse::class)->success('A mail has been sent to your account!');
        } catch (TransportExceptionInterface $e) {
            Log::error($e);
            DB::rollback();

            return app(ApiResponse::class)->error('Something went wrong.');
        }
    }


    private function isValidToken($token): bool
    {
        try {
            $httpClient = new \GuzzleHttp\Client();
            $httpClient->get('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='. $token);

            return true;
        } catch(ClientException $e) {
            Log::error($e);

            return false;
        }
    }
}
